#!/bin/bash

read -p "Introduce un número del que quieras saber su tabla: " num

for (( i=1; i<=10; i++ ))
do
	echo "$num x $i = $(( num * $i ))"
done
